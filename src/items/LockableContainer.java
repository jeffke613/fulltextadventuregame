package items;

import interfaces.Lockable;
import textadventure.Player;
import textadventure.World;

public class LockableContainer extends CloseableContainer implements Lockable{

	private boolean isLocked;
	private Item key;
	public LockableContainer(World world, String name, String description, boolean _isOpen, boolean isLocked) {
		super(world, name, description, _isOpen);
		this.setLocked(isLocked);
		// TODO Auto-generated constructor stub
	}

	

	public void setKey(Item key) {
		this.key = key;
	}

	@Override
	public void doOpen() {
		// TODO Auto-generated method stub
		if(isLocked())
		{
			World.print("It\'s locked.\n\n");
			return;
		}
		if(isOpen())
		{
			World.print("It\'s already open.\n\n");
			return;
		}
		super.doOpen();
	}
	@Override
	public void doClose() {
		// TODO Auto-generated method stub
		if(!isOpen())
		{
			World.print("It\'s already closed.\n\n");
			return;
		}
		super.doClose();
	}
	public void doUnlock() {
		// TODO Auto-generated method stub
		if(!isLocked())
		{
			World.print("It\'s already unlocked.\n\n");
			return;
		}
		Player player = getWorld().getPlayer();
		if(!player.hasItem(key))
		{
			World.print("You don\'t have the key!\n\n");
			return;
		}
		setLocked(Lockable.UNLOCKED);
		World.print("Unlocked.\n\n");
	}

//	public void setKey(Item key)
//	{
//		this.key = key;
//	}
	@Override
	public boolean isLocked() {
		// TODO Auto-generated method stub
		return isLocked;
	}

	@Override
	public void doLock() {
		// TODO Auto-generated method stub
		if(isLocked())
		{
			World.print("It\'s already locked.\n\n");
			return;
		}
		Player player = getWorld().getPlayer();
		if(!player.hasItem(key))
		{
			World.print("You don\'t have the key!\n\n");
			return;
		}
		setLocked(Lockable.LOCKED);
		doClose();
		World.print("Locked.\n\n");
	}

	public Item getKey() {
		return key;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

}
