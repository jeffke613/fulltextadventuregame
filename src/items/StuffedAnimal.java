package items;

import textadventure.World;

public class StuffedAnimal extends Container{

	private Item itemDrop;
	public StuffedAnimal(World world, String name, String description, Item itemDrop) {
		super(world, name, 0, Item.NOT_TAKEABLE, description);
		this.itemDrop = itemDrop;
		// TODO Auto-generated constructor stub
	}

	
	public void cut()
	{
		World.print("You cut open the " + getName() + " with your glass shard.\nThe poor stuffed animal falls over, torn apart and out from view.\n\n");
		getWorld().getPlayer().getCurrentRoom().addItem(itemDrop);
		World.print("A " + itemDrop.getName() + " falls out.\n\n");
	}
	@Override
	public String toString()
	{
		return getName();
	}
	@Override
	public void doExamine() {
		// TODO Auto-generated method stub
		super.doExamine();
		World.print("You notice something bulging inside of it though. You might need something sharp to <cut> it open.\n\n");
	}
	

}
