package command;

import items.GlassShard;
import items.Item;
import items.StuffedAnimal;
import textadventure.Player;
import textadventure.Room;
import textadventure.World;

public class CommandCut extends Command{


	@Override
	public String[] getCommandWords() {
		// TODO Auto-generated method stub
		return new String[] {"cut", "slash"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		// cut [StuffedAnimal] with [Shard]
		if(params.length == 3)
		{
			if(!params[1].equals("with"))
			{
				World.print("Invalid Syntax: <cut> [thing] with [sharp_object]\n\n");
				return;
			}
			String stuffedName = params[0];
			Room curRoom = world.getPlayer().getCurrentRoom();
			Player player = world.getPlayer();
			String shardName = params[2];
			if(!player.hasItem(shardName))
			{
				World.print("You don't have the " + shardName + "\n\n");
				return;
			}
			if(!player.hasItem(stuffedName) && !curRoom.hasItem(stuffedName))
			{
				World.print("You don\'t see any " + stuffedName + "\n\n");
				return;
			}
			Item shard = (curRoom.hasItem(shardName)) ? curRoom.getItem(shardName) : player.getItem(shardName);
			if(!(shard instanceof GlassShard))
			{
				World.print("You can\'t cut with that!\n\n");
				return;
			}
			Item stuffed = (player.hasItem(stuffedName)) ? player.getItem(stuffedName) : curRoom.getItem(stuffedName);
			if(!(stuffed instanceof StuffedAnimal))
			{
				World.print("You can\'t cut that open!\n\n");
				return;
			}
			((StuffedAnimal)stuffed).cut();
			world.getPlayer().getCurrentRoom().removeItem(stuffedName);
		}
		else
		{
			World.print("Invalid Syntax: <cut> [thing] with [sharp_object]\n\n");
		}
		
	}

	@Override
	public String getHelpDescription() {
		// TODO Auto-generated method stub
		return "[thing] with [sharp_object]";
	}

}
