package textadventure;

public class OutsideRoom extends Room {

	public OutsideRoom(String name, String description, World world) {
		super(name, description, world);
		// TODO Auto-generated constructor stub
	}

	public OutsideRoom(String name, String description, boolean isLocked, World world) {
		super(name, description, isLocked, world);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doEnter() {
		// TODO Auto-generated method stub
		World.print("Your carpool rolls up to the curb. \n\n");
		Player player = getWorld().getPlayer();
		if(!player.getHasBrushedTeeth())
		{
			World.print("Your carpool mates notice that you haven't brushed your teeth.  How embarrassing!  Game over!");
			System.exit(0);
		}
		if(player.getHealth() < 2)
		{
			World.print("Your stomach churns as you realize you forgot to toast the bread.  You get sick in on the way to school.  How embarrassing!  Game over!");
			System.exit(0);
		}
		if(!player.getIsWearingClothes())
		{
			World.print("You forgot your clothes! Your carpool buddies look at you strangely and you feel embarrassed. Game over!");
			System.exit(0);
		}
		World.print("You have a great day at school. YOU WIN!!");
		System.exit(0);
	}
}
