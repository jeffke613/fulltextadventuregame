package items;

import textadventure.Dumpster;
import textadventure.World;

public class PopsicleStick extends Item {

	public PopsicleStick(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
		// TODO Auto-generated constructor stub
	}

	public PopsicleStick(World world, String name, int weight, String description) {
		super(world, name, weight, description);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doUse() {
		// TODO Auto-generated method stub
		if(!getWorld().getPlayer().getCurrentRoom().hasItem("wax_paper"))
		{
			World.print("You swing the stick around, but nothing else happens. \n\n");
			return;
		}
		else
		{
			World.print("Using the stick you pry the wax paper off the ground. You find a piece of rancid chewing gum.\n\n");
			getWorld().getPlayer().getCurrentRoom().addItem(new Secret(getWorld(), "rancid_gum", 0, Item.TAKEABLE, "This is a piece of"
					+ " rancid, greenish chewing gum. Some strange grey spots crust its surface. You glance at the exposed underside"
					+ " of the wax paper and read the words \'AXED-HEAD: CANNABIS CHEWS\'."));

			
		}
	}

}
