package command;

import interfaces.Edible;
import items.Food;
import items.Item;
import textadventure.Player;
import textadventure.World;

public class CommandEat extends Command {
	
	@Override
	public String[] getCommandWords() {
		// TODO Auto-generated method stub
		return new String[] {"eat"};
	}
	
	@Override
	public void doCommand(String cmd, String[] params, World world) {
		// TODO Auto-generated method stub
		if(params.length != 1)
		{
			World.print("What do you want to eat?\n\n");
			return;
		}
		Player player = world.getPlayer();
		if(!player.hasItem(params[0]))
		{
			World.print("You don't have the " + params[0] + "\n\n");
			return;
		}
		else
		{
			Item food = player.getItem(params[0]);
			if(food instanceof Edible)
			{
				((Food) food).doEat();
				World.print("Eaten.\n\n");
			}
			else
			{
				World.print("That\'s plainly inedible!\n\n");
			}
		}
				
	}
	
	@Override
	public String getHelpDescription() {
		// TODO Auto-generated method stub
		return "[item]";
	}
}
