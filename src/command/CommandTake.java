
package command;

import items.ChildDrawing;
import items.Container;

import items.Item;
import items.Secret;
import textadventure.*;

public class CommandTake extends Command {

	@Override
	public String[] getCommandWords() {
		return new String[]{"take", "get", "grab", "hold"};
	}
	
	

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		Player player = world.getPlayer();
		if(player.getItems().size() >= 4)
		{
			World.print("You don\'t have enough space to fit that many items in your milk carton! Better drop some items...\n\n");
			return;
		}
		if (params.length == 1) 
		{
			String itemName = params[0];
			
			if (world.getPlayer().getCurrentRoom().hasItem(itemName))
			{
				
				Item item = world.getPlayer().getCurrentRoom().getItem(itemName);
				
				if(item instanceof Secret)
				{
					((MilkCarton)player).incrementSecrets();
					World.print("You found a secret!\n\n");
					World.print(item.getDescription()+ "\n\n");
					World.print("The secret mysteriously disappears.\n\n");
					world.getPlayer().getCurrentRoom().removeItem(item);
					if(((MilkCarton)player).getNumSecrets() == 5)
					{
						World.print("You\'ve found all of the secrets! Type <quit> to exit if you\'re done exploring the dumpster. You also think you hear some clamoring and shifting in the distance.\n\n");
						((Dumpster)world).generateCokeCan();
						return;
					}
					World.print("You\'ve found " + ((MilkCarton)player).getNumSecrets() + " of the 5 secrets!\n\n");
					return;
				}
				if(item instanceof ChildDrawing)
				{
					if(world.getPlayer().hasItem("glass_shard"))
					{
						World.print("You examine the drawing closely: " + item.getDescription() + " You quicky glance down at your"
								+ " glass shard in your hand. You look in your reflection, and you come to the sudden realization "
								+ "that the rectangular figure is a drawing of YOU! That green shirt is almost a mirror image of your "
								+ "green label: Crystal Creamery: Cool Cow Low Fat Milk. The triangular head was just the shape of your "
								+ "opened head! The realization that you were the precipitation of a figure of Kevin’s imagination fills "
								+ "you with a strange sense of warmth, that even in Kevin\'s dark times, he still thought of his old childhood"
								+ " friend: an old, used milk carton. \n\n");
						World.print("You got ENDING: GOOD. You found all the secrets to this family\'s tragedy, as well as discern the"
								+ " mystery behind your peculiar existence. Good job!");
						System.exit(0);
					}
					else
					{
						World.print("You examine the drawing closely: " + item.getDescription() + " The drawing makes no sense, however."
								+ " You are filled with mild disgruntlement. You throw the drawing to the ground and wander around the "
								+ "dumpster, a sense of dread filling your stomach as you desperately search the dumpster for more clues"
								+ ", but find nothing. You can\'t help but feel that the answer was right in front of you.\n\n");
						World.print("You got ENDING: BAD. You did indeed find the secrets to this family\'s tragedy, but the entire reason"
								+ " for your entire being still remains a dreadful mystery. Better luck next time!");
						System.exit(0);
					}
				}
				item.doTake(world.getPlayer().getCurrentRoom());
			} 
			else if (world.getPlayer().hasItem(itemName)) 
			{
				World.print("You already have that!\n\n");
			} 
			else {
				World.print("You can't see any " + itemName + " here.\n\n");
			}
		}
		// Handle "take [Item] from [Container]" command
		else if(params.length == 3) 
		{
			// Handle errors
			if (!params[1].equals("from"))
			{
				world.print("I don't understand." + "\n\n");
				return;
			}
			if (!world.getPlayer().hasItem(params[2]) && !world.getPlayer().getCurrentRoom().hasItem(params[2]))
			{
				world.print("You can't see any " + params[2] + " here." + "\n\n");
				return;
			}
			if (!(world.getPlayer().getItem(params[2]) instanceof Container) && !(world.getPlayer().getCurrentRoom().getItem(params[2]) instanceof Container))
			{
				world.print("The " + params[2] + " can\'t hold things!" + "\n\n");
				return;
			}
			Container container;
//			System.out.println(world.getPlayer().hasItem(params[2]));
//			System.out.println(world.getPlayer().getCurrentRoom().hasItem(params[2]));
			container = (world.getPlayer().hasItem(params[2])) ? ((Container)world.getPlayer().getItem(params[2])) : ((Container)world.getPlayer().getCurrentRoom().getItem(params[2]));
			if (!container.hasItem(params[0]))
			{
				world.print("The " + params[2] + " doesn't have a " + params[0]);
				return;
			}
		
		// If we made it this far, then it's safe to
		// take [item] from [container]
			Item item = container.getItem(params[0]);
			if(item instanceof Secret)
			{
				((MilkCarton)player).incrementSecrets();
				World.print("You found a secret!\n\n");
				World.print(item.getDescription()+ "\n\n");
				World.print("The secret mysteriously disappears.\n\n");
				World.print("You have found " + ((MilkCarton)player).getNumSecrets() + " of the 5 secrets!\n\n");
				container.removeItem(item);
				if(((MilkCarton)player).getNumSecrets() == 5)
				{
					World.print("You\'ve found all of the secrets! Type <quit> to exit if you\'re done exploring the dumpster. You also think you hear some clamoring and shifting in the distance.\n\n");
					((Dumpster)world).generateCokeCan();
					return;
				}
				return;
			}
			container.doTake(item);
		}

		else {
			World.print("I don't understand.\n\n");
		}
	}

	@Override
	public String getHelpDescription() {
		return "[item] or [item] from [container]";
	}

}
