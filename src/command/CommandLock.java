package command;

import items.Item;
import items.LockableContainer;
import textadventure.Player;
import textadventure.Room;
import textadventure.World;

public class CommandLock extends Command {

	@Override
	public String[] getCommandWords() {
		// TODO Auto-generated method stub
		return new String[] {"lock"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		// TODO Auto-generated method stub
		//unlock [LockableContainer] with [key]
		if(params.length == 3)
		{
			String lockName = params[0];
			Room curRoom = world.getPlayer().getCurrentRoom();
			Player player = world.getPlayer();
			if(!params[1].equals("with"))
			{
				World.print("Invalid Syntax.\n\n");
				return;
			}
			if(!curRoom.hasItem(lockName) && !player.hasItem(lockName)) 
			{
				World.print("You can't see any " + lockName + " here.\n\n");
				return;
			}
			Item locked = (curRoom.hasItem(lockName)) ? (Item) curRoom.getItem(lockName) : curRoom.getItem(lockName);
			if(!(locked instanceof LockableContainer))
			{
				World.print("You can't lock that!\n\n ");
				return;
			}
			if(((LockableContainer)locked).isLocked())
			{
				World.print("It\'s already locked!\n\n");
				return;
			}
			((LockableContainer)locked).doLock();
		}
		else 
		{
			World.print("Invalid Syntax.\n\n");
		}
			
	}

	@Override
	public String getHelpDescription() {
		// TODO Auto-generated method stub
		return "[LockableContainer] with [key]";
	}

}
