package textadventure;

import items.Item;
import items.Trophy;

public class TrophyRoom extends Room {

	private Trophy red;
	private Trophy blue;
	private Trophy white;
	public TrophyRoom(String name, String description, World world) {
		super(name, description, world);
		red = new Trophy(world, "red_trophy", 0, Item.NOT_TAKEABLE, "A large metal trophy with a black rectangular base with a metal miniature figure of a martial artist kicking the air standing on top of it. The figure has a dull red finish, and is peeling in multiple places. Upon closer inspection you realize the head of the miniature is crushed flat. The words 2nd Place Inter-County Power Breaking Competition 6.23.02 are engraved on the black stone.");
		blue = new Trophy(world, "blue_trophy", 0, Item.NOT_TAKEABLE, "A very small blue trophy. A golden disc sits upright on a small black base. A picture of a black belt is emblazoned on the disc. The image is a little smudged. A small plaque reads Awarded to Kevin Simmons for Participation in the Sparring Tournament 10.25.98.");
		white = new Trophy(world, "white_trophy", 0, Item.NOT_TAKEABLE, "A trophy in the shape of a tarnished, patchy silver goblet. It has a wide crack that marrs its reflective surface. To your disgust you find a dusty cigarette butt in the bottom of the cup. The words Kevin Simmons: 18th Place Sunset County Breaking Tournament 3.29.03 mark a golden plate affixed to the bottom of the goblet.");
		red.setPosition(Trophy.KNOCKED_OVER);
		blue.setPosition(Trophy.KNOCKED_OVER);
		white.setPosition(Trophy.KNOCKED_OVER);
		this.addItem(red);
		this.addItem(blue);
		this.addItem(white);
		// TODO Auto-generated constructor stub
	}

	public TrophyRoom(String name, String description, boolean isLocked, World world) {
		super(name, description, isLocked, world);
		// TODO Auto-generated constructor stub
	}

	public void checkRoom()
	{
		boolean trophy1 = red.getPosition() == Trophy.CENTER;
		boolean trophy2 = blue.getPosition() == Trophy.LEFT;
		boolean trophy3 = white.getPosition() == Trophy.RIGHT;
		if(trophy1 && trophy2 && trophy3)
		{
			((Dumpster)getWorld()).generate2ndSection();
		}
		else
		{
			World.print("You feel as if the trophies are still out of order.\n\n");
		}
			
	}
}
