package textadventure;

import interfaces.Closeable;
import interfaces.Lockable;
//import items.Clothes;
import items.*;
import items.*;

public class Dumpster extends World {

	
	public static final String ALLEY1 = "TIGHT ALLEYWAY";
	public static final String ALLEY2 = "TIGHT AND NARROW ALLEYWAY";
	public static final String ALLEY3 = "CLAUSTROPHOBIC ALLEYWAY";
	public static final String ALLEY4 = "TIGHT AND SMALL ALLEYWAY";
	public static final String ALLEYA= "SMALL ALLEYWAY";
	public static final String ALLEYB = "WIDE AND LONG ALLEYWAY";
	public static final String ALLEYC = "LONG ALLEYWAY";
	public static final String ALLEYD = "LARGE ALLEYWAY";
	public static final String ALLEYE = "DARK ALLEYWAY";
	public static final String ALLEYF = "DIM ALLEYWAY";
	public static final String ALLEYG = "SMELLY ALLEYWAY";
	public static final String ALLEYH = "MUSTY ALLEYWAY";
	public static final String ALLEYI = "QUIET ALLEYWAY";
	public static final String ALLEYJ = "ALLEYWAY";
	public static final String ROUND_BOX = "A ROUND CARDBOARD BOX";
	public static final String MOLDY_BOX = "A MOLDY WET BOX";
	public static final String CHINESE_BOX = "A CHINESE TAKE-OUT BOX";
	public static final String SHOEBOX = "A HARD CARDBOARD SHOEBOX";
	public static final String SAFEROOM = "A BLACK CARDBOARD BOX";
	public static final String COKE = "A PARTIALLY CRUSHED COKE CAN";
	public static Item PLASTICKEY; 
	
	public Dumpster() {
		// TODO Auto-generated constructor stub
	}

	public void createPlayer() {
		setPlayer(new MilkCarton(getRoom(ALLEY1), this, "Milk Carton", "You feel strange, as if your body feels heavy and hollow."));
		
	}
	@Override
	public void printWelcome() {
		// TODO Auto-generated method stub
		World.print("You are a milk carton who has magically sprouted legs and a yearning desire to explore. "
				+ "You were thrown into the dumpster of a high school kid and his family. The dumpster is large"
				+ " and treacherous: you must be careful to not get lost among the trash and the refuse."
				+ " Because you only have a milk carton’s worth of space, you can’t hold that many things. "
				+ "Piece together pieces of the family’s life and "
				+ "tragedy through pieces of their trash, and unlock the secrets of your peculiar existence\nType <help> to get help.\n\n");
		World.print("It is strongly recomended that you make your own map of the area! You are, after all, just a small milk carton lost in the depths of a huge dumpster.\n\n");
		World.print("You have found 0 of the 5 secrets.\n");
		World.print("\n" + getPlayer().getCurrentRoom().getDescription() + "\n\n");
	}
	
	public void createRooms()
	{
		PLASTICKEY = new UselessItem(this, "plastic_key", 0, Item.TAKEABLE, "This is a bright pink plastic key. It\'s gotta be paired with some kind of toy locket or something.");
		addRoom(new Room(ALLEY2, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		addRoom(new Room(ALLEY1, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		addRoom(new Room(ALLEYE, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		
		addRoom(new Room(ALLEYA, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		addRoom(new Room(ALLEYB, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		addRoom(new Room(ALLEYC, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		addRoom(new Room(ALLEYD, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		
		addRoom(new Room(ROUND_BOX, "This is a round cardboard box in the shape of a cylinder. The edges of the box are worn. The words kevin\'s old toys mark the side. The ink is faded, however, and hard to read.", this));
		addRoom(new Room(CHINESE_BOX, "A greasy white box with chunky red stains on every side, which you carefully step over as you walk through. A rancid odor secretes from some hidden corner of the box you can’t identify.", this));
		//make this moldy_box its own class so I can integrate some new features. 
		addRoom(new TrophyRoom(MOLDY_BOX, "\'kevin’s TKD trophies\'. The wall’s own weight causes protruding "
				+ "creases and crumples within itself. Dark spots of water splotches the wall."
				+ " The trophies somehow look out of order. There are empty spots to the left, center, and the right."
				+ "\n\nYou think you can <move> the trophies. Type <help> if you\'re stuck on the syntax.", this));
//		
		getRoom(ALLEY1).setExits(getRoom(ALLEYA), getRoom(ALLEYB), null, getRoom(ALLEYC));
		getRoom(ALLEY1).addItem(new PopsicleStick(this, "popsicle_stick", 0, Item.TAKEABLE, "This is a short brown popsicle stick with teeth marks along its surface. It looks like a good tool to pry something off the ground."));
//		getRoom(ALLEY1).addItem(new PasswordContainer(this, "test", "test locked container", 942903));
		getRoom(ALLEYA).setExit("south", getRoom(ALLEY1));
		getRoom(ALLEYB).setExit("west", getRoom(ALLEY1));
		getRoom(ALLEYC).setExit("east", getRoom(ALLEY1));
		
		getRoom(ALLEYC).setExit("south", getRoom(ALLEY2));
		getRoom(ALLEY2).setExit("north", getRoom(ALLEYC));
		getRoom(ALLEY1).addItem(new GlassShard(this, "glass_shard", 0, Item.TAKEABLE, "This is a very shiny glass shard. Looking at it you can see your own reflection. You are indeed a used milk carton with legs."));
//		getRoom(ALLEY1).addItem(new PhotoPiece(this, "test_piece1", 0, Item.TAKEABLE, "test"));
//		getRoom(ALLEY1).addItem(new PhotoPiece(this, "test_piece2", 0, Item.TAKEABLE, "test"));
//		getRoom(ALLEY1).addItem(new PhotoPiece(this, "test_piece3", 0, Item.TAKEABLE, "test"));
//		getRoom(ALLEY1).addItem(new PhotoPiece(this, "test_piece4", 0, Item.TAKEABLE, "test"));
		getRoom(ALLEY2).addItem(new Container(this, "pile_of_dirt", "This is a pile dirt and other rubbish."));
		((Container)getRoom(ALLEY2).getItem("pile_of_dirt")).addItem(new PhotoPiece(this, "torn_white_paper", 0, Item.TAKEABLE, "It is a torn piece of paper with strange black and white shapes and blobs on it. You have no idea what to make of it."));
		((Container)getRoom(ALLEY2).getItem("pile_of_dirt")).addItem(PLASTICKEY);
		getRoom(ALLEYB).setExit("south", getRoom(ROUND_BOX));
		getRoom(ROUND_BOX).setExit("north", getRoom(ALLEYB));
		
		getRoom(ALLEYB).setExit("east", getRoom(ALLEYD));
		getRoom(ALLEYD).setExit("west", getRoom(ALLEYB));
		
		getRoom(ALLEYD).setExit("north", getRoom(ALLEYE));
		getRoom(ALLEYE).setExit("south", getRoom(ALLEYD));
		
		getRoom(ALLEYA).setExit("west", getRoom(CHINESE_BOX));
		getRoom(ALLEYA).addItem(new GiantRat(getRoom(ALLEYA), this, "disgusting_rat", "This is a huge disgusting rat. It has long yellow claws and beady black eyes. Its black fur is ragged and patched. It looks around ferociously, blocking a box to the east. It looks very, very hungry. Maybe you could drop something for it to eat?"));
		getRoom(ALLEYA).setExit("east", getRoom(MOLDY_BOX));
		getRoom(MOLDY_BOX).doLock();
		getRoom(CHINESE_BOX).setExit("east", getRoom(ALLEYA));
		getRoom(CHINESE_BOX).addItem(new Food(this, "nasty_pork", 0, Item.TAKEABLE, "It looks disgusting; only the the most depraved of predators would even consider this a meal."));
		getRoom(CHINESE_BOX).addItem(new PhotoPiece(this, "torn_black_paper", 0, Item.TAKEABLE, "It is a torn piece of paper with strange black and white shapes and blobs on it. You have no idea what to make of it."));
		getRoom(CHINESE_BOX).addItem(new UselessItem(this, "wax_paper", 0, Item.NOT_TAKEABLE, "This is a sticky wad of wax paper stuck to the ground. "
				+ "You see a strange bump under the paper, but you think you need something to pry the paper off the ground."));
		
		
		getRoom(MOLDY_BOX).setExit("west", getRoom(ALLEYA));
		
		getRoom(ROUND_BOX).addItem(new StuffedAnimal(this, "pink_elephant", "This is a very cute pink elephant. ", new UselessItem(this, "fluff", 0, Item.TAKEABLE, "Some fluffy white fluff.")));
		getRoom(ROUND_BOX).addItem(new StuffedAnimal(this, "pink_rat", "This is a very cute pink rat. ", new UselessItem(this, "fluff", 0, Item.TAKEABLE, "Some fluffy white fluff.")));
		getRoom(ROUND_BOX).addItem(new StuffedAnimal(this, "pink_snake", "This is a very long pink snake. ", new UselessItem(this, "fluff", 0, Item.TAKEABLE, "Some fluffy white fluff.")));
		getRoom(ROUND_BOX).addItem(new StuffedAnimal(this, "pink_hedgehog", "This is a very cute pink hedgehog. ", new UselessItem(this, "fluff", 0, Item.TAKEABLE, "Some fluffy white fluff.")));
		getRoom(ROUND_BOX).addItem(new StuffedAnimal(this, "pink_owl", "This is a very cute pink owl. ", new Secret(this, "drawing", 0, Item.TAKEABLE, "A crudely drawn yet seemingly thoughtful crayola picture. "
				+ "The title is mommy and me. It’s a picture of a smiling young boy holding his mother’s hand. His mother’s mouth is flat, almost frowning. They’re standing outside a simply drawn house with a "
				+ "triangle roof. Inside the house is a tall smiling man wearing a white triangle gown. The words \'by kevin s\' mark the bottom of the artwork.\n")));
		
		getRoom(ROUND_BOX).addItem(new StuffedAnimal(this, "pink_penguin", "This is a very cute pink penguin.", new PhotoPiece(this, "torn_smudged_paper", 0, Item.TAKEABLE, "\"It is a torn piece of paper with strange black and white shapes and blobs on it. You have no idea what to make of it.")));
//		generateCokeCan();
		
	}

	public void generate2ndSection()
	{
		World.print("You arrange the trophies in the correct order. Suddenly you feel the whole dumpster shift and rumble.\n\n An exit to the north appears! \n\n");
		addRoom(new Room(ALLEYF, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		addRoom(new Room(ALLEYG, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		addRoom(new Room(ALLEYH, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		addRoom(new Room(ALLEYI, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		addRoom(new Room(ALLEYJ, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		addRoom(new Room(SAFEROOM, "This is a hard black cardboard box. Its walls are scuffed with scratches, and the cheap black paints rubs off with your touch.", this));
//		addRoom(new Room(ALLEY1, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		addRoom(new Room(ALLEY3, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		addRoom(new Room(ALLEY4, "You are in an alleyway between boxes. It is narrow, and your cardboard sides brush against the walls. \n",this));
		addRoom(new Room(SHOEBOX, "kevin’s old math tests. This is a reddish-orange nike shoebox, with a large white check-mark on its lid. The inside is rather clean, and moisture free.", this));
		getRoom(SHOEBOX).addItem(new LockableContainer(this, "heart_locket", "This is a small pink toy locket. Though it’s made of plastic, the lock on it looks very sturdy.", Closeable.CLOSED, Lockable.LOCKED));
		((LockableContainer)getRoom(SHOEBOX).getItem("heart_locket")).setKey(PLASTICKEY);
		((LockableContainer)getRoom(SHOEBOX).getItem("heart_locket")).addItem(new PhotoPiece(this, "torn_grey_paper", 0, Item.TAKEABLE, "It is a torn piece of paper with strange black and white shapes and blobs on it. You have no idea what to make of it."));
		getRoom(SHOEBOX).addItem(new Container(this, "paper_stack", 0, Item.NOT_TAKEABLE, "This is a large stack of white papers, kept together by a rusty paperclip. You think you can look through the stack. "));
		((Container)getRoom(SHOEBOX).getItem("paper_stack")).addItem(new UselessItem(this, "flat_paper", 0, Item.TAKEABLE, "This is a crumpled white paper. The numbers 86/90 mark the top of the paper. As you glance further at the paper, you realize this is an algebra test. Numbers and variables are typed out on the paper in small black ink, with grey scrawls of scratchwork surrounding them. You see a small date written at the bottom of the paper: 1.19.03"));
		((Container)getRoom(SHOEBOX).getItem("paper_stack")).addItem(new UselessItem(this, "clean_paper", 0, Item.TAKEABLE, "This is a clean white paper. The numbers 45/46 mark the top of the paper. You read through the paper, and you realize that this is also an algebra test. You see a small date written at the bottom of the paper: 2.02.03"));
		((Container)getRoom(SHOEBOX).getItem("paper_stack")).addItem(new UselessItem(this, "wrinkled_paper", 0, Item.TAKEABLE, "This is a wrinkled white paper. The numbers 52/60 mark the top of the paper. You read through the paper, and find out this is yet another math test. You’re beginning to see a pattern here. A small date is written on the bottom of the paper: 3.05.03."));
		((Container)getRoom(SHOEBOX).getItem("paper_stack")).addItem(new UselessItem(this, "crumpled_paper", 0, Item.TAKEABLE, "This is a white paper that’s been very badly crumpled then flattened as if it were a second thought. The numbers 77/100 mark the top of the paper. A small date is written on the bottom of the paper: 4.01.03."));
		((Container)getRoom(SHOEBOX).getItem("paper_stack")).addItem(new UselessItem(this, "ripped_paper", 0, Item.TAKEABLE, "This is a white paper that’s been torn in multiple places, then taped back together. The numbers 44/90 mark the top of the paper. A small date is written on the bottom of the paper: 4.28.03."));
		((Container)getRoom(SHOEBOX).getItem("paper_stack")).addItem(new UselessItem(this, "blank_paper", 0, Item.TAKEABLE, "This is a white paper that for some reason has no work done on it: it’s completely blank save for the questions written in black ink. The numbers 0/50 mark the top of the paper. The words written in all-caps SEE ME IN MY OFFICE are right beside the numbers. A small date is written on the bottom of the paper: 5.13.03"));
		((Container)getRoom(SHOEBOX).getItem("paper_stack")).addItem(new CloseableContainer(this, "thick_envelope", "This is a professional looking white envelope. The return address is partially smudged off:  BROWN COLLEGE PREPARATORY", Closeable.CLOSED));
		((CloseableContainer)((Container)getRoom(SHOEBOX).getItem("paper_stack")).getItem("thick_envelope")).addItem(new Secret(this, "ripped_letter", 0, Item.TAKEABLE, "Dear Mr. and Mrs. Simmons,\n" + 
				"I regret to inform you that your son Kevin has been expelled indefinitely after six cans of beer were found in his locker. This was his third strike after the last incide- [the letter ends here after a rip in the paper]"));
//		((CloseableContainer)((Container)getRoom(SHOEBOX).getItem("paper_stack")).getItem("thick_envelope")).addItem(new PhotoPiece(this, "torn_black_paper", 0, Item.TAKEABLE, "Another piece of paper with strange black and white blobs on it. You don’t know what to make of it."));
		getRoom(ALLEYF).setExit("south", getRoom(MOLDY_BOX));
		getRoom(MOLDY_BOX).setExit("north", getRoom(ALLEYF));
		
		getRoom(ALLEYF).setExit("west", getRoom(ALLEYG));
		getRoom(ALLEYG).setExit("east", getRoom(ALLEYF));
		
		getRoom(ALLEYF).setExit("north", getRoom(SHOEBOX));
		getRoom(SHOEBOX).setExit("south", getRoom(ALLEYF));
		
		getRoom(SHOEBOX).setExit("east", getRoom(ALLEYI));
		getRoom(ALLEYI).setExit("west", getRoom(SHOEBOX));
		
		getRoom(ALLEY3).setExit("south", getRoom(ALLEYI));
		getRoom(ALLEYI).setExit("north", getRoom(ALLEY3));
		
		getRoom(SHOEBOX).setExit("west", getRoom(ALLEYH));
		getRoom(ALLEYH).setExit("east", getRoom(SHOEBOX));
		
		getRoom(ALLEYH).setExit("west", getRoom(ALLEYJ));
		getRoom(ALLEYJ).setExit("east", getRoom(ALLEYH));
		
		getRoom(ALLEYJ).setExit("north", getRoom(SAFEROOM));
		getRoom(SAFEROOM).setExit("south", getRoom(ALLEYJ));
		
		getRoom(ALLEYJ).setExit("south", getRoom(ALLEY4));
		getRoom(ALLEY4).setExit("north", getRoom(ALLEYJ));
		
		getRoom(SAFEROOM).addItem(new PasswordContainer(this, "metal_safe", "This is a cheap metal safe. You can tell how thin the walls are by how hollow it sounds when you knock on it. It’s made of dark black sheets of metal. On its front is a six-digit number lock. You vaguely remember the password having something to do with a date.", 10, 29, 13));
		((Container)getRoom(SAFEROOM).getItem("metal_safe")).addItem(new Secret(this, "folded_paper", 0, Item.TAKEABLE, "This is a folded letter. It\'s addressed to a woman named Mrs. Simmons.\nMEDICAL BILLING STATEMENT: PAST DUE. For Your Hospital Services: 3.20.03 - 3.28.03\n" + 
				"CURRENT CHARGES: \n" + 
				"Medical/Surgical Supplies and Devices: $6,721.25\n" + 
				"Intensive Care Unit: $153,332.00\n" + 
				"CAT Scan: $1,243.67\n" + 
				"Chemotherapy: $5,674.89\n" + 
				"Chemotherapy: $2,354.29\n" + 
				"Chemotherapy: $6,574.23\n" + 
				"Chemotherapy: $9,532.44\n" + 
				"Chemotherapy: $7,284.12\n" + 
				"Chemotherapy: $8,729.23\n" + 
				"Chemotherapy: $8,960.34\n" + 
				"Chemotherapy: $5,682.49\n" + 
				"Chemotherapy: $7,294.12\n" + 
				"Chemotherapy: $8,293.14\n" + 
				"Chemotherapy: $8,291.78\n" + 
				"Professional Fee: $55,325.98\n" + 
				"Other: $78,907.24\n" + 
				"Total: $163,794.75\n"));
	}
	
	public void generateCokeCan()
	{
		addRoom(new Room(COKE, "This is a partially crushed, dirty coke can.", this));
		getRoom(COKE).setExit("north", getRoom(ALLEY1));
		getRoom(ALLEY1).setExit("south", getRoom(COKE));
		//add the ending sequence.
		getRoom(COKE).addItem(new ChildDrawing(this, "child_drawing", 0, Item.TAKEABLE, "This is child’s drawing, drawn in bright green, black, and red crayon. A scrawling portrait of a small boy drawn in flaky black crayon holding hands with a some sort of rectangular figure. It’s got some sort of green shirt and a triangular head"));
		
	}
	@Override
	public void onCommandFinished() {
		// TODO Auto-generated method stub

	}

	@Override
	public void initializeGame() {
		// TODO Auto-generated method stub
		
		createRooms();
		createPlayer();
		
	}

}
