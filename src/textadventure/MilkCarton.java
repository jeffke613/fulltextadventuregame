package textadventure;

import items.ChildDrawing;
import items.Item;
import items.Secret;

public class MilkCarton extends Player {

	private int numSecrets;
	public MilkCarton(Room startRoom, World world, String name, String description) 
	{
		super(startRoom, world, name, description);

		numSecrets = 0;
	}
	public void incrementSecrets()
	{
		numSecrets++;
	}
	
	
	public int getNumSecrets() {
		return numSecrets;
	}

}
