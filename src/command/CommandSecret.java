package command;

import textadventure.MilkCarton;
import textadventure.World;

public class CommandSecret extends Command {

	public CommandSecret() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] getCommandWords() {
		// TODO Auto-generated method stub
		return new String[] {"secret", "secrets"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		// TODO Auto-generated method stub
		World.print("You have found " + ((MilkCarton)world.getPlayer()).getNumSecrets() + " of 5 total secrets.\n\n");
	}

	@Override
	public String getHelpDescription() {
		// TODO Auto-generated method stub
		return null;
	}

}
