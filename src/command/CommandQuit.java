package command;

import textadventure.MilkCarton;
import textadventure.World;

public class CommandQuit extends Command {

	@Override
	public String[] getCommandWords() {
		return new String[] {"quit"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		World.print("\nAre you sure you want to quit (enter y to confirm)? ");
		if (World.IN.nextLine().equalsIgnoreCase("y")) {
			if(((MilkCarton)world.getPlayer()).getNumSecrets() == 6)
			{
				World.print("You got ENDING 0: You found all the secrets this family had in their dumpster, but the secret"
						+ " of your peculiar existence (you\'re a walking milk carton for pete\'s sake!) still remains obscure."
						+ " Better luck next time!");
			}
			else
			{
				World.print("You got ENDING -1: You didn\'t find all the secrets this family had in their dumpster, and the secret"
						+ " of your peculiar existence (you\'re a walking milk carton for pete\'s sake!) still remains obscure."
						+ " Better luck next time!");
			}
			World.print("\nThank you for playing!  Good bye!\n\n");
			System.exit(0);
		}
	}

	@Override
	public String getHelpDescription() {
		return "";
	}

}
