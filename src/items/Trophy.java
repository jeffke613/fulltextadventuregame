package items;

import textadventure.World;

public class Trophy extends Item {

	public static final int LEFT = -1;
	public static final int CENTER = 0;
	public static final int RIGHT = 1;
	public static final int KNOCKED_OVER = Integer.MIN_VALUE;
	private int position;
	public Trophy(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
		// TODO Auto-generated constructor stub
	}

	public Trophy(World world, String name, int weight, String description) {
		super(world, name, weight, description);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doUse() {
		// TODO Auto-generated method stub
		World.print("You have no idea on how to use this trophy.\n\n");
		
	}
	
	public int getPosition()
	{
		return position;
	}
	
	public void setPosition(int position)
	{
		this.position = position;
	}
	
	@Override
	public void doExamine() {
		// TODO Auto-generated method stub
		super.doExamine();
		if(position == Trophy.KNOCKED_OVER)
		{
			World.print("Right now this trophy is knocked over. \n\n");
			return;
		}
		String strPos;
		if(position == Trophy.LEFT)
		{
			strPos = "left";
		}
		else if(position == Trophy.CENTER)
		{
			strPos = "center";
		}
		else
		{
			strPos = "right";
		}
		World.print("Right now its position is in the " + strPos + "\n\n");
	}

}
