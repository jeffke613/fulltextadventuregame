package command;

import items.Item;
import items.Trophy;
import textadventure.Player;
import textadventure.Room;
import textadventure.TrophyRoom;
import textadventure.World;
/**
 * This method is exclusively for the
 * TrophyRoom as a puzzle to unlock
 * more rooms. 
 * @author jeffreyke
 *
 */
public class CommandMove extends Command {

	public CommandMove() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] getCommandWords() {
		// TODO Auto-generated method stub
		return new String[] {"move"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		// TODO Auto-generated method stub
		//move [trophy] to [position]
		if(params.length == 3)
		{
			if(!params[1].equals("to"))
			{
				World.print("Invalid Syntax.\n\n");
				return;
			}
			String pos = params[2];
			if(!(pos.equals("left") || pos.equals("center") || pos.equals("right")))
			{
				World.print("That\'s not a valid position! Use [left/center/right]\n\n");
				return;
			}
			Room curRoom = world.getPlayer().getCurrentRoom();
			Player player = world.getPlayer();
			String trophy = params[0];
			if(!(player.hasItem(trophy) || curRoom.hasItem(trophy)))
			{
				World.print("You don\'t see any " + trophy + "\n\n");
				return;
			}
			Item item = (curRoom.hasItem(trophy)) ? curRoom.getItem(trophy) : player.getItem(trophy);
			if(!(item instanceof Trophy))
			{
				World.print("You can\'t move that.\n\n");
				return;
			}
			int position;
			if(pos.equals("left"))
			{
				position = Trophy.LEFT;
			}
			else if(pos.equals("right"))
			{
				position = Trophy.RIGHT;
			}
			else
			{
				position = Trophy.CENTER;
			}
			((Trophy)item).setPosition(position);
			World.print("You move the " + item.getName() + " to the " + pos + ".\n");
			((TrophyRoom)curRoom).checkRoom();
		}
		else
		{
			World.print("Invalid Syntax.\n\n");
		}
	}

	@Override
	public String getHelpDescription() {
		// TODO Auto-generated method stub
		return "[object] to [left/right/center]";
	}

}
