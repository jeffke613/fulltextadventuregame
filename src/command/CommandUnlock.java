package command;

import items.Item;
import items.LockableContainer;
import items.PasswordContainer;
import textadventure.Player;
import textadventure.Room;
import textadventure.World;

public class CommandUnlock extends Command {

	@Override
	public String[] getCommandWords() {
		// TODO Auto-generated method stub
		return new String[] {"unlock"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		// TODO Auto-generated method stub
		//unlock [LockableContainer] with [key]
		if(params.length == 1)
		{
			String lockName = params[0];
			Room curRoom = world.getPlayer().getCurrentRoom();
			Player player = world.getPlayer();
			if(!curRoom.hasItem(lockName) && !player.hasItem(lockName)) 
			{
				World.print("You can't see any " + lockName + " here.\n\n");
				return;
			}
			Item locked = (curRoom.hasItem(lockName)) ? (Item) curRoom.getItem(lockName) : curRoom.getItem(lockName);
			if(!(locked instanceof PasswordContainer))
			{
				World.print("This doesn\'t seem like something you can unlock without a key or unlock at all.\n\n");
				return;
			}
			((PasswordContainer)locked).doUse();
		}
		else if(params.length == 3)
		{
			String lockName = params[0];
			Room curRoom = world.getPlayer().getCurrentRoom();
			Player player = world.getPlayer();
			String keyName = params[2];
			if(!params[1].equals("with"))
			{
				World.print("Invalid Syntax.\n\n");
				return;
			}
			if(!player.hasItem(keyName))
			{
				World.print("You don\'t have any " + keyName + ".\n\n");
				return;
			}
			if(!curRoom.hasItem(lockName) && !player.hasItem(lockName)) 
			{
				World.print("You can't see any " + lockName + " here.\n\n");
				return;
			}
			Item locked = (curRoom.hasItem(lockName)) ? (Item) curRoom.getItem(lockName) : curRoom.getItem(lockName);
			if(!(locked instanceof LockableContainer))
			{
				World.print("You can't unlock that!\n\n ");
				return;
			}
			if(!player.getItem(keyName).equals(((LockableContainer)locked).getKey()))
			{
				World.print("That\'s not the key for that!\n\n");
				return;
			}
			if(!((LockableContainer)locked).isLocked())
			{
				World.print("It\'s already unlocked!\n\n");
				return;
			}
			((LockableContainer)locked).doUnlock();
		}
		else 
		{
			World.print("Invalid Syntax.\n\n");
		}
			
	}

	@Override
	public String getHelpDescription() {
		// TODO Auto-generated method stub
		return "[LockableContainer] with [key]";
	}

}
