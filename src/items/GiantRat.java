package items;

import textadventure.Dumpster;
import textadventure.Player;
import textadventure.Room;
import textadventure.World;

public class GiantRat extends Player {

	public GiantRat(Room startRoom, World world, String name, String description) {
		super(startRoom, world, name, description);
		startRoom.removeExit("east");
		// TODO Auto-generated constructor stub
	}
	
	public void eat(Food food)
	{
		World.print("The giant rat devours the " + food.getName() + ". \n\n");
		getCurrentRoom().removeItem(food);
		getWorld().getRoom(Dumpster.MOLDY_BOX).doUnlock();
		World.print("He scurries away, opening the exit to the east.\n\n");
		getWorld().getRoom(Dumpster.ALLEYA).removeItem(this);
	}

}
