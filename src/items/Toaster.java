package items;

import textadventure.World;

public class Toaster extends Container {

	public Toaster(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void doUse() {
		// TODO Auto-generated method stub
		if(this.getItems().size() > 1)
		{
			World.print("There are too many items in the " + getName() + "\n\n");
			return;
		}
		if(this.getItems().size() == 0)
		{
			World.print("There's nothing in the " + getName() + " to toast\n\n");
			return;
		}
		
		if(this.getItems().get(0).getName().equals("bread"))
		{
			World.print("You toast the bread, transforming it into...\n\n");
			this.removeItem("bread");
			this.addItem(new Food(getWorld(), "toast", 2, Item.TAKEABLE, "Nice crunchy toast"));
			
			
		}
		else
		{
			World.print("You attempt to toast the " + this.getItems().get(0).toString() + " and burn the house down! "
					+ "Game over :(\n\n");
			System.exit(0);
		}
	}

}
