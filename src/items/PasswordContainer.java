package items;

import java.util.InputMismatchException;
import java.util.Scanner;

import interfaces.Closeable;
import interfaces.Lockable;
import textadventure.World;

public class PasswordContainer extends LockableContainer {

	private int first2;
	private int secon2;
	private int third2;
	public PasswordContainer(World world, String name, String description, int first2, int secon2, int third2) {
		super(world, name, description, Closeable.CLOSED, Lockable.LOCKED);
		this.first2 = first2;
		this.secon2 = secon2;
		this.third2 = third2;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void doUse()
	{
		try
		{
			Scanner scan = new Scanner(System.in);
			World.print("The first two digits: ");
			int pass1 = scan.nextInt();
			scan.nextLine();
			World.print("\n");
			boolean firstTrue = false;
			boolean secondTrue = false;
			boolean thirdTrue = false;
			if(first2 == pass1)
			{
				World.print("You flip the digits for the first two numbers.\n\n");
				firstTrue = true;
			}
			World.print("The second two digits: ");
			int pass2 = scan.nextInt();
			World.print("\n");
			if(secon2 == pass2)
			{
				World.print("You flip the digits for the second two numbers.\n\n");
				secondTrue = true;
			}
			World.print("The third two digits: ");
			int pass3 = scan.nextInt();
			World.print("\n");
			if(third2 == pass3)
			{
				World.print("You flip the digits for the third two numbers.\n\n");
				thirdTrue = true;
			}
			
			if(firstTrue && secondTrue && thirdTrue)
			{
				World.print("The lock clicks open.\n\n");
				this.doUnlock();
			}
			else
			{
				World.print("You flip the digits but nothing happens. It\'s probably the wrong password.\n\n");
			}
			
			//scan.close();
		}
		catch(InputMismatchException bruh)
		{
			World.print("\n");
			World.print("That\'s not really a password.\n\n");
		}
	}
	@Override
	public void doUnlock()
	{
		setLocked(Lockable.UNLOCKED);
	}
	
}
