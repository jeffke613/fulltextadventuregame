package command;

import items.Item;
import items.PhotoPiece;
import items.Secret;
import textadventure.Player;
import textadventure.World;
/**
 * This combine method is essentially just for just for combining the four
 * picture parts to create the last secret.
 * @author jeffreyke
 *
 */
public class CommandCombine extends Command {

	public CommandCombine() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] getCommandWords() {
		// TODO Auto-generated method stub
		return new String[] {"combine"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		// TODO Auto-generated method stub
		if(params.length == 4)
		{
			String i1 = params[0];
			String i2 = params[1];
			String i3 = params[2];
			String i4 = params[3];
			Player p = world.getPlayer();
			if(!(p.hasItem(i1) || p.hasItem(i2) || p.hasItem(i3) || p.hasItem(i4)))
			{
				World.print("You don\'t have one or more of those items.\n\n");
				return;
			}
			
			Item j1 = p.getItem(i1);
			Item j2 = p.getItem(i2);
			Item j3 = p.getItem(i3);
			Item j4 = p.getItem(i4);
			
			if(!(j1 instanceof PhotoPiece) || !(j2 instanceof PhotoPiece) || !(j3 instanceof PhotoPiece) || !(j4 instanceof PhotoPiece))
			{
				World.print("You try to combine those items, but nothing happens.\n\n");
				return;
			}
			p.removeItem(j1);
			p.removeItem(j2);
			p.removeItem(j3);
			p.removeItem(j4);
			
			World.print("You combine all the pieces of paper. Sticking the pieces together with bits of milk from yourself, you suddenly realize that this"
					+ " is actually an old photo!\n\n The photo drops to the ground.\n\n");
			world.getPlayer().getCurrentRoom().addItem(new Secret(world, "old_photo", 0, Item.TAKEABLE, "This is an old dogeared photo"
					+ " with greying edges. You look closer at the image and you see a family of three. You see a short grey-haired woman"
					+ " holding a slouching, hooded teenager's shoulders. The woman is smiling in the photo, but the teenager is staring"
					+ " at the ground. The woman\'s face looks wrinkled, but it could just be the wrinkles in the photo due to age. "
					+ "They\'re standing in front of what looks like a snow-capped mountain in the distance. You flip the photo over"
					+ " and see a date scribbled: 10.29.13. You think this is a VERY important date somehow. "));
			
		}
		else
		{
			World.print("You can\'t combine those.");
			return;
		}
	}

	@Override
	public String getHelpDescription() {
		// TODO Auto-generated method stub
		return "[object] [object] [object] [object]";
	}

}
